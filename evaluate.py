import argparse
import os
import pdb

import numpy as np

import torch

from tabulate import tabulate

from metrics import METRICS

from utils import chunk_by_size


DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def load_predictions(dataset_key, model_key):
    filename = os.path.join("predictions", dataset_key, model_key + ".npy")
    with open(filename, "rb") as f:
        log_probas = np.load(f)
        nr_frames = np.load(f)
        targets = np.load(f)
    log_probas = torch.tensor(log_probas).to(DEVICE)
    return log_probas, nr_frames, targets


def main():
    parser = argparse.ArgumentParser(
        description="Evaluates a given model in terms of given metrics."
    )
    parser.add_argument(
        "-m", "--model-key", type=str, required=True, help="the key of the model"
    )
    parser.add_argument(
        "-d", "--dataset-key", type=str, required=True, help="the key of the dataset"
    )
    parser.add_argument(
        "--metrics",
        type=str,
        required=True,
        choices=METRICS,
        nargs="+",
        help="which metric to use for evaluation",
    )
    parser.add_argument("--fmt", help="table format")
    args = parser.parse_args()

    STR_FMT = {
        'eer': '{:.3f}',
        'error': '{:.1f}',
        'loss': '{:.2f}',
        'sent': '{:.2f}',
    }

    metrics = {m: METRICS[m] for m in args.metrics}
    log_probas_concat, nr_frames, targets = load_predictions(args.dataset_key, args.model_key)
    log_probas_iter = chunk_by_size(log_probas_concat, nr_frames)

    for log_probas, target in zip(log_probas_iter, targets):
        for metric in metrics.values():
            metric.update(log_probas, target)

    results = [STR_FMT[m].format(metrics[m].compute()) for m in args.metrics]
    table = [results]
    print(tabulate(table, headers=args.metrics, tablefmt=args.fmt))

    if "eer" in metrics:
        metrics["eer"].plot()


if __name__ == "__main__":
    main()
