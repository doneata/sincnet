import os
import random

import numpy as np

from toolz.itertoolz import take


BASE_PATH = os.path.expanduser("~/data")


class TimitDataset:
    NR_CLASSES = 462
    SPLITS = {"train": "train", "valid": "test", "test": "test"}

    def __init__(self, split):
        self.path_src = os.path.join(BASE_PATH, "timit-luc")
        self.path_aux = os.path.join(BASE_PATH, "timit-aux")
        self.split = split
        self.file_names = self.get_file_names()

    def __str__(self):
        return "timit-" + self.split

    def get_file_names(self):
        split = self.SPLITS[self.split]
        path = os.path.join(self.path_aux, "TIMIT_{}.scp".format(split))
        with open(path, "r") as f:
            return {line.strip() for line in f.readlines()}

    def get_paths_and_targets(self):
        e = lambda p: os.path.join(self.path_src, p)
        path = os.path.join(self.path_aux, "TIMIT_labels.npy")
        items = np.load(path).item().items()
        return [(e(p), t) for p, t in items if p in self.file_names]


class Timit10Dataset(TimitDataset):
    """A subset of the TIMIT dataset based on only 10 speakers."""

    NR_CLASSES = 10
    SELECTED_CLASSES = [287, 130, 179, 211, 358, 56, 434, 158, 122, 443]

    def __init__(self, split):
        super(Timit10Dataset, self).__init__(split)
        self.selected_classes_dict = {c: i for i, c in enumerate(self.SELECTED_CLASSES)}

    def __str__(self):
        return "timit10-" + self.split

    def get_paths_and_targets(self):
        paths_and_targets = super(Timit10Dataset, self).get_paths_and_targets()
        return [
            (path, self.selected_classes_dict[target])
            for path, target in paths_and_targets
            if target in self.selected_classes_dict
        ]


class TimitVal(TimitDataset):
    """TIMIT dataset with a validation set."""

    SPLITS = {"train": "train", "valid": "train", "test": "test"}

    def __init__(self, split):
        # Extra information for filtering file-names for the validation set.
        self.PREDS = {
            "train": lambda i: i % 5,
            "valid": lambda i: i % 5 == 0,
            "test": lambda _: True,
        }
        super(TimitVal, self).__init__(split)

    def __str__(self):
        return "timitval-" + self.split

    def get_file_names(self):
        pred = self.PREDS[self.split]
        split = self.SPLITS[self.split]
        path = os.path.join(self.path_aux, "TIMIT_{}.scp".format(split))
        with open(path, "r") as f:
            return {line.strip() for i, line in enumerate(f.readlines()) if pred(i)}


class LibriSpeechVal:
    """LibriSpeech dataset with a validation set."""

    NR_CLASSES = 2484
    SPLITS = {"train": "tr", "valid": "tr", "test": "te"}

    def __init__(self, split):
        # Extra information for filtering file-names for the validation set.
        self.PREDS = {
            "train": lambda _: True,
            "valid": lambda _: True,
            "test": lambda _: True,
        }
        self.path_src = os.path.join(BASE_PATH, "librispeech", "Librispeech_spkid_sel")
        self.path_aux = os.path.join(BASE_PATH, "librispeech", "Librispeech_spk_id")
        self.split = split
        self.file_names = self.get_file_names()

    def __str__(self):
        return "librival-" + self.split

    def get_file_names(self):
        pred = self.PREDS[self.split]
        split = self.SPLITS[self.split]
        path = os.path.join(self.path_aux, "libri_{}.scp".format(split))
        with open(path, "r") as f:
            return {line.strip() for i, line in enumerate(f.readlines()) if pred(i)}

    def get_paths_and_targets(self):
        e = lambda p: os.path.join(self.path_src, p)
        path = os.path.join(self.path_aux, "libri_dict.npy")
        items = np.load(path).item().items()
        return [(e(p), t) for p, t in items if p in self.file_names]


DATASETS = {
    "timit": TimitDataset,
    "timit-10": Timit10Dataset,
    "timit-val": TimitVal,
    "libri-val": LibriSpeechVal,
}
