"""A one-dimensional ResNet-like architecutre"""
import math
import pdb

from hyperopt import hp

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.autograd import Variable

from models.utils import LayerNorm1D


DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def conv1(in_planes, out_planes, stride=1):
    return nn.Conv1d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


def conv3(in_planes, out_planes, stride=1):
    return nn.Conv1d(
        in_planes, out_planes, kernel_size=3, stride=stride, padding=1, bias=False
    )


def flip(x, dim):
    xsize = x.size()
    dim = x.dim() + dim if dim < 0 else dim
    x = x.contiguous()
    x = x.view(-1, *xsize[dim:])
    i = torch.arange(x.size(1) - 1, -1, -1).long()
    x = x.view(x.size(0), x.size(1), -1)[:, i, :]
    return x.view(xsize)


def sinc(band, t_right):
    y_right = torch.sin(2 * math.pi * band * t_right) / (2 * math.pi * band * t_right)
    y_left = flip(y_right, 0)
    ones = torch.ones(1).to(DEVICE)
    y = torch.cat([y_left, ones, y_right])
    return y


class SincConv(nn.Module):
    def __init__(self, nr_filters, filter_size, freq, stride, **kargs):
        super(SincConv, self).__init__()

        # Mel initialization of the filterbanks
        freq_mel_low = 80
        freq_mel_high = 2595 * np.log10(1 + (freq / 2) / 700)  # Convert Hz to Mel

        # Equally spaced in Mel scale
        mel_points = np.linspace(freq_mel_low, freq_mel_high, nr_filters)

        f_cos = 700 * (10 ** (mel_points / 2595) - 1)  # Convert Mel to Hz

        b1 = np.roll(f_cos, +1)
        b2 = np.roll(f_cos, -1)

        b1[0] = 30
        b2[-1] = (freq / 2) - 100

        self.freq_scale = freq * 1.0

        self.filt_b1 = nn.Parameter(torch.from_numpy(b1 / self.freq_scale))
        self.filt_band = nn.Parameter(torch.from_numpy((b2 - b1) / self.freq_scale))

        self.nr_filters = nr_filters
        self.filter_size = filter_size
        self.freq = freq
        self.stride = stride

    def forward(self, x):
        filters = Variable(torch.zeros((self.nr_filters, self.filter_size)))
        filters = filters.to(DEVICE)

        N = self.filter_size
        steps = int((N - 1) / 2)

        t_right = Variable(torch.linspace(1, (N - 1) / 2, steps=steps) / self.freq)
        t_right = t_right.to(DEVICE)

        min_freq = 50.0
        min_band = 50.0

        filt_beg_freq = torch.abs(self.filt_b1) + min_freq / self.freq_scale
        filt_end_freq = filt_beg_freq + (
            torch.abs(self.filt_band) + min_band / self.freq_scale
        )

        n = torch.linspace(0, N, steps=N)

        # Filter window (hamming)
        window = 0.54 - 0.46 * torch.cos(2 * math.pi * n / N)
        window = Variable(window.float())
        window = window.to(DEVICE)

        for i in range(self.nr_filters):

            low_pass1 = (
                2
                * filt_beg_freq[i].float()
                * sinc(filt_beg_freq[i].float() * self.freq_scale, t_right)
            )
            low_pass2 = (
                2
                * filt_end_freq[i].float()
                * sinc(filt_end_freq[i].float() * self.freq_scale, t_right)
            )

            band_pass = low_pass2 - low_pass1
            band_pass = band_pass / torch.max(band_pass)

            filters[i, :] = band_pass * window

        padding = (self.filter_size - 1) // 2
        out = F.conv1d(
            x,
            filters.view(self.nr_filters, 1, self.filter_size),
            stride=self.stride,
            padding=padding,
        )

        return out


class ResidualBlock(nn.Module):
    expansion = 1

    def __init__(self, in_planes, planes, stride=1, downsample=None):
        super(ResidualBlock, self).__init__()
        self.conv1 = conv3(in_planes, planes, stride)
        self.bn1 = nn.BatchNorm1d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3(planes, planes)
        self.bn2 = nn.BatchNorm1d(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class Agafya(nn.Module):
    def __init__(self, input_params, output_params, hyper_params):
        super(Agafya, self).__init__()
        self.RESIDUAL_PARAMS = [(64, 1), (128, 2), (256, 2), (512, 2)]

        # Extract parameters
        layers = hyper_params["residual"]
        nr_classes = output_params["nr-classes"]
        nr_fc_features = self.RESIDUAL_PARAMS[len(layers) - 1][0]
        self.in_planes = hyper_params["conv1"]["nr_filters"]

        self.inputnorm = self._make_inputnorm(
            hyper_params["input"]["type_"], input_params
        )
        self.sinc = SincConv(**hyper_params["sinc"], **input_params)
        self.conv1 = self._make_conv1(hyper_params)
        self.bn1 = nn.BatchNorm1d(hyper_params["conv1"]["nr_filters"])
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool1d(kernel_size=3, stride=2, padding=1)
        self.residual = nn.Sequential(*self._make_residual(hyper_params))
        self.avgpool = nn.AdaptiveAvgPool1d((1,))
        self.fc = nn.Linear(nr_fc_features * ResidualBlock.expansion, nr_classes)
        self.log_soft = nn.LogSoftmax(dim=1)

    def _make_inputnorm(self, type_, input_params):
        if type_ == "standardization":

            def norm(X):
                μ = -4.2707e-06
                σ = +0.0181
                return (X - μ) / σ

        elif type_ == "layer-norm-1d":
            norm = LayerNorm1D((1, input_params["duration-frames"]))
        else:
            assert False, "unknown normalization type"
        return norm

    def _make_conv1(self, hyper_params):
        in_channels = hyper_params["sinc"]["nr_filters"]
        out_channels = hyper_params["conv1"]["nr_filters"]
        kernel_size = hyper_params["conv1"]["kernel_size"]
        if kernel_size > 5:
            stride = (kernel_size - 3) // 2
        else:
            stride = 1
        padding = (kernel_size - 1) // 2
        return nn.Conv1d(
            in_channels,
            out_channels,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            bias=False,
        )

    def _make_residual(self, hyper_params):
        params = zip(hyper_params["residual"], *zip(*self.RESIDUAL_PARAMS))
        return [
            self._make_layer(ResidualBlock, nr_channels, nr_blocks, stride)
            for nr_blocks, nr_channels, stride in params
        ]

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.in_planes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1(self.in_planes, planes * block.expansion, stride),
                nn.BatchNorm1d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.in_planes, planes, stride, downsample))
        self.in_planes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.in_planes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        b, n = x.shape
        x = x.view(b, 1, n)
        x = self.inputnorm(x)
        x = self.sinc(x)
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)
        x = self.residual(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        x = self.log_soft(x)
        return x


class Agape(nn.Module):
    def __init__(self, input_params, output_params, hyper_params):
        super(Agape, self).__init__()
        self.RESIDUAL_PARAMS = [(64, 1), (128, 2), (256, 2), (512, 2)]

        # Extract parameters
        layers = hyper_params["residual"]
        nr_classes = output_params["nr-classes"]
        nr_fc_features = self.RESIDUAL_PARAMS[len(layers) - 1][0]
        self.in_planes = hyper_params["conv1"]["nr_filters"]

        self.inputnorm = self._make_inputnorm(
            hyper_params["input"]["type_"], input_params
        )
        self.conv1 = self._make_conv1(hyper_params)
        self.bn1 = nn.BatchNorm1d(hyper_params["conv1"]["nr_filters"])
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool1d(kernel_size=3, stride=2, padding=1)
        self.residual = nn.Sequential(*self._make_residual(hyper_params))
        self.avgpool = nn.AdaptiveAvgPool1d((1,))
        self.fc = nn.Linear(nr_fc_features * ResidualBlock.expansion, nr_classes)
        self.log_soft = nn.LogSoftmax(dim=1)

    def _make_inputnorm(self, type_, input_params):
        if type_ == "standardization":

            def norm(X):
                μ = -4.2707e-06
                σ = +0.0181
                return (X - μ) / σ

        elif type_ == "layer-norm-1d":
            norm = LayerNorm1D((1, input_params["duration-frames"]))
        else:
            assert False, "unknown normalization type"
        return norm

    def _make_conv1(self, hyper_params):
        in_channels = 1
        out_channels = hyper_params["conv1"]["nr_filters"]
        kernel_size = hyper_params["conv1"]["kernel_size"]
        stride = hyper_params["conv1"].get("stride", 3)
        padding = (kernel_size - 1) // 2
        return nn.Conv1d(
            in_channels,
            out_channels,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            bias=False,
        )

    def _make_residual(self, hyper_params):
        params = zip(hyper_params["residual"], *zip(*self.RESIDUAL_PARAMS))
        return [
            self._make_layer(ResidualBlock, nr_channels, nr_blocks, stride)
            for nr_blocks, nr_channels, stride in params
        ]

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.in_planes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1(self.in_planes, planes * block.expansion, stride),
                nn.BatchNorm1d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.in_planes, planes, stride, downsample))
        self.in_planes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.in_planes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        b, n = x.shape
        x = x.view(b, 1, n)
        x = self.inputnorm(x)
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)
        x = self.residual(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        x = self.log_soft(x)
        return x


class UtilsAgafya:
    hyper_param_space = {
        # window
        # "window.size:factor": hp.quniform("window.size:factor", 1, 4, 1),
        # input
        "input.normalization": hp.choice(
            "input_normalization", ["standardization", "layer-norm-1d"]
        ),
        # "input.normalization": hp.choice("input.normalization", ["layer-norm-1d"]),
        # sinc
        "sinc.nr_filters": hp.quniform("sinc_nr_filters", 64, 128, 1),
        "sinc.filter_size:half": hp.quniform("sinc_filter_size_half", 32, 256, 4),
        "sinc.stride:half": hp.quniform("sinc_stride_half", 0, 15, 1),  # 1 3 5 ... 31
        # first covolution after sinc
        "conv1.nr_filters:log2": hp.quniform(
            "conv1_nr_filters_log2", 6, 9, 1
        ),  # 64 128 256 512
        "conv1.filter_size:half": hp.quniform(
            "conv1_filter_size_half", 1, 4, 1
        ),  # 3 5 7 9
        # residual layers
        "residual_layers": hp.choice(
            "residual_layers",
            [
                {
                    "nr_layers": 2,
                    "nr_blocks_0": hp.quniform("nr_blocks_0_2", 1, 4, 1),
                    "nr_blocks_1": hp.quniform("nr_blocks_1_2", 1, 4, 1),
                },
                {
                    "nr_layers": 3,
                    "nr_blocks_0": hp.quniform("nr_blocks_0_3", 1, 4, 1),
                    "nr_blocks_1": hp.quniform("nr_blocks_1_3", 1, 4, 1),
                    "nr_blocks_2": hp.quniform("nr_blocks_2_3", 1, 4, 1),
                },
                {
                    "nr_layers": 4,
                    "nr_blocks_0": hp.quniform("nr_blocks_0_4", 1, 4, 1),
                    "nr_blocks_1": hp.quniform("nr_blocks_1_4", 1, 4, 1),
                    "nr_blocks_2": hp.quniform("nr_blocks_2_4", 1, 4, 1),
                    "nr_blocks_3": hp.quniform("nr_blocks_3_4", 1, 4, 1),
                },
            ],
        ),
        # optimization
        "optim.learning_rate:log10": hp.uniform(
            "optim_learning_rate_log10", -4, -1.5
        ),  # [1e-4, 1e-1.5]
    }

    @staticmethod
    def hyper_param_space_to_model_space(hyper_choice):
        return {
            "input": {"type_": hyper_choice["input.normalization"]},
            "sinc": {
                "nr_filters": int(hyper_choice["sinc.nr_filters"]),
                "filter_size": 2 * int(hyper_choice["sinc.filter_size:half"]) + 1,
                "stride": 2 * int(hyper_choice["sinc.stride:half"]) + 1,
            },
            "conv1": {
                "nr_filters": 2 ** int(hyper_choice["conv1.nr_filters:log2"]),
                "kernel_size": 2 * int(hyper_choice["conv1.filter_size:half"]) + 1,
            },
            "residual": [
                int(hyper_choice["residual_layers"]["nr_blocks_{}".format(r)])
                for r in range(hyper_choice["residual_layers"]["nr_layers"])
            ],
        }


class UtilsAgape:
    hyper_param_space = {
        # window
        # "window.size:factor": hp.quniform("window.size:factor", 1, 4, 1),
        # input
        "input.normalization": hp.choice(
            "input_normalization", ["standardization", "layer-norm-1d"]
        ),
        # "input.normalization": hp.choice("input.normalization", ["layer-norm-1d"]),
        # first covolution after sinc
        # TODO Add stride
        "conv1.nr_filters:log2": hp.quniform(
            "conv1_nr_filters_log2", 6, 9, 1
        ),  # 64 128 256 512
        "conv1.filter_size:half": hp.quniform(
            "conv1_filter_size_half", 1, 9, 1
        ),  # 3 5 7 9 11 13 15 17 19
        "conv1.stride:half": hp.quniform("conv1_stride_half", 0, 2, 1),  # 1 3 5
        # residual layers
        "residual_layers": hp.choice(
            "residual_layers",
            [
                {
                    "nr_layers": 2,
                    "nr_blocks_0": hp.quniform("nr_blocks_0_2", 1, 4, 1),
                    "nr_blocks_1": hp.quniform("nr_blocks_1_2", 1, 4, 1),
                },
                {
                    "nr_layers": 3,
                    "nr_blocks_0": hp.quniform("nr_blocks_0_3", 1, 4, 1),
                    "nr_blocks_1": hp.quniform("nr_blocks_1_3", 1, 4, 1),
                    "nr_blocks_2": hp.quniform("nr_blocks_2_3", 1, 4, 1),
                },
                {
                    "nr_layers": 4,
                    "nr_blocks_0": hp.quniform("nr_blocks_0_4", 1, 4, 1),
                    "nr_blocks_1": hp.quniform("nr_blocks_1_4", 1, 4, 1),
                    "nr_blocks_2": hp.quniform("nr_blocks_2_4", 1, 4, 1),
                    "nr_blocks_3": hp.quniform("nr_blocks_3_4", 1, 4, 1),
                },
            ],
        ),
        # optimization
        "optim.learning_rate:log10": hp.uniform(
            "optim_learning_rate_log10", -4, -1.5
        ),  # [1e-4, 1e-1.5]
    }

    @staticmethod
    def hyper_param_space_to_model_space(hyper_choice):
        return {
            "input": {"type_": hyper_choice["input.normalization"]},
            "conv1": {
                "nr_filters": 2 ** int(hyper_choice["conv1.nr_filters:log2"]),
                "kernel_size": 2 * int(hyper_choice["conv1.filter_size:half"]) + 1,
                "stride": 2 * int(hyper_choice["conv1.stride:half"]) + 1,
            },
            "residual": [
                int(hyper_choice["residual_layers"]["nr_blocks_{}".format(r)])
                for r in range(hyper_choice["residual_layers"]["nr_layers"])
            ],
        }
