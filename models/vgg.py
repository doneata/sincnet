import torch.nn as nn

from torch.nn.modules.normalization import LayerNorm

from models.utils import LayerNorm1D


def make_layers(cfg, use_batch_norm=False):
    layers = []
    in_channels = 1
    for v in cfg:
        if v == "M":
            layers.append(nn.MaxPool2d(kernel_size=2, stride=2))
        else:
            layers.append(nn.Conv2d(in_channels, v, kernel_size=3, padding=1))
            if use_batch_norm:
                layers.append(nn.BatchNorm2d(v))
            layers.append(nn.ReLU())
            in_channels = v
    return nn.Sequential(*layers)


def make_layers_1d(layer_config, use_batch_norm=False, input_normalization=""):
    layers = []
    input_size = (60, 118)
    nr_channels = input_size[0]
    # Input normalization
    if input_normalization == "layer-norm":
        layers.append(LayerNorm(input_size))
    elif input_normalization == "layer-norm-1d":
        layers.append(LayerNorm1D(input_size))
    # Convolutional layers
    for v in layer_config:
        if v == "M":
            layers.append(nn.MaxPool1d(kernel_size=2, stride=2))
        else:
            layers.append(nn.Conv1d(nr_channels, v, kernel_size=3, padding=1))
            if use_batch_norm:
                layers.append(nn.BatchNorm1d(v))
            layers.append(nn.ReLU())
            nr_channels = v
    return nn.Sequential(*layers)
