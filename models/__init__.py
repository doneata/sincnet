import os
import pdb

from torch import nn

from torch.nn import Sequential

from torch.nn.modules.normalization import LayerNorm

from models.sincmlp import SincNet, MLP

from models import agafya, resnet, tcn, vgg


MODEL_PATH = "model-weights"


def get_model_path(key):
    return os.path.join(MODEL_PATH, key + ".pt")


def make_classifier(input_size, nr_classes):
    return nn.Sequential(
        nn.Linear(input_size, 512),
        nn.ReLU(inplace=True),
        nn.Dropout(0.2),
        nn.Linear(512, nr_classes),
        nn.LogSoftmax(dim=1),
    )


class SincNetVGG(nn.Module):
    def __init__(self, input_params, output_params, hyper_params):
        super(SincNetVGG, self).__init__()
        hyper_params["sinc"]["fs"] = input_params["freq"]
        hyper_params["sinc"]["input_dim"] = input_params["duration-frames"]
        self.sinc = SincNet(hyper_params["sinc"])
        self.vgg = vgg.make_layers(hyper_params["vgg"], use_batch_norm=True)
        self.classifier = make_classifier(
            # TODO Replace hardcoded values with something along the following lines:
            # → nr_channels * nr_sinc_filters / 2^m * window_len / 2^m
            # where `m` is the number of max pooling layers
            input_size=512 * 3 * 7,
            nr_classes=output_params["nr-classes"],
        )

    def forward(self, x):
        x = self.sinc(x)
        x = x.unsqueeze(1)
        x = self.vgg(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x


class SincNetVGG1D(nn.Module):
    def __init__(self, input_params, output_params, hyper_params):
        super(SincNetVGG1D, self).__init__()
        hyper_params["sinc"]["fs"] = input_params["freq"]
        hyper_params["sinc"]["input_dim"] = input_params["duration-frames"]
        self.sinc = SincNet(hyper_params["sinc"])
        self.vgg = vgg.make_layers_1d(**hyper_params["vgg"])
        self.classifier = make_classifier(
            # TODO Replace hardcoded values with something along the following lines:
            # → nr_channels * nr_sinc_filters / 2^m * window_len / 2^m
            # where `m` is the number of max pooling layers
            input_size=512 * 7,
            nr_classes=output_params["nr-classes"],
        )

    def forward(self, x):
        x = self.sinc(x)
        x = self.vgg(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x


class SincNetMLP(nn.Module):
    def __init__(self, input_params, output_params, hyper_params):
        super(SincNetMLP, self).__init__()
        # TODO Is there a way to avoid manually specifying the input dimensions?
        hyper_params["sinc"]["fs"] = input_params["freq"]
        hyper_params["sinc"]["input_dim"] = input_params["duration-frames"]
        self.sinc = SincNet(hyper_params["sinc"])
        hyper_params["mlp1"]["input_dim"] = self.sinc.out_dim
        self.mlp1 = MLP(hyper_params["mlp1"])
        hyper_params["mlp2"]["input_dim"] = hyper_params["mlp1"]["fc_lay"][-1]
        hyper_params["mlp2"]["fc_lay"] = [output_params["nr-classes"]]
        self.mlp2 = MLP(hyper_params["mlp2"])

    def forward(self, x):
        x = self.sinc(x)
        x = x.view(x.size(0), -1)
        x = self.mlp1(x)
        x = self.mlp2(x)
        return x


def make_mlp(
    input_size, activation="relu", use_batch_norm=False, use_layer_norm_input=False
):
    layers = []

    if use_layer_norm_input:
        layers.append(LayerNorm(input_size))

    for d in [512, 512, 512]:
        layers.append(nn.Linear(input_size, d))

        if activation == "relu":
            layers.append(nn.ReLU())
        elif activation == "leaky-relu":
            layers.append(nn.LeakyReLU(0.2))
        else:
            assert False, "ERROR Unknown activation type: {}".format(activation)

        if use_batch_norm:
            layers.append(nn.BatchNorm1d(d))

        input_size = d

    return nn.Sequential(*layers)


class SincNetMLP2(nn.Module):
    def __init__(self, input_params, output_params, hyper_params):
        super(SincNetMLP2, self).__init__()
        hyper_params["sinc"]["fs"] = input_params["freq"]
        hyper_params["sinc"]["input_dim"] = input_params["duration-frames"]
        self.sinc = SincNet(hyper_params["sinc"])
        self.mlp = make_mlp(60 * 118, **hyper_params["mlp"])
        self.classifier = make_classifier(
            input_size=512, nr_classes=output_params["nr-classes"]
        )

    def forward(self, x):
        x = self.sinc(x)
        x = x.view(x.size(0), -1)
        x = self.mlp(x)
        x = self.classifier(x)
        return x


class SincNetResNet(nn.Module):
    def __init__(self, input_params, output_params, hyper_params):
        super(SincNetResNet, self).__init__()
        hyper_params["sinc"]["fs"] = input_params["freq"]
        hyper_params["sinc"]["input_dim"] = input_params["duration-frames"]
        self.sinc = SincNet(hyper_params["sinc"])
        self.resnet = resnet.make_layers(**hyper_params["resnet"])
        self.classifier = make_classifier(
            input_size=512 * 8, nr_classes=output_params["nr-classes"]
        )

    def forward(self, x):
        x = self.sinc(x)
        x = self.resnet(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x


class SincNetTCN(nn.Module):
    def __init__(self, input_params, output_params, hyper_params):
        super(SincNetTCN, self).__init__()
        hyper_params["sinc"]["fs"] = input_params["freq"]
        hyper_params["sinc"]["input_dim"] = input_params["duration-frames"]
        self.sinc = SincNet(hyper_params["sinc"])
        self.ln = LayerNorm((60, 118))
        self.tcn = tcn.TemporalConvNet(**hyper_params["tcn"])
        self.classifier = nn.Sequential(
            nn.Linear(
                hyper_params["tcn"]["num_channels"][-1], output_params["nr-classes"]
            ),
            nn.LogSoftmax(dim=1),
        )

    def forward(self, x):
        x = self.sinc(x)
        x = self.ln(x)
        x = self.tcn(x)
        x = x[:, :, -1]
        x = self.classifier(x)
        return x


MODELS = {
    "sinc-mlp": SincNetMLP,
    "sinc-mlp2": SincNetMLP2,
    "sinc-vgg": SincNetVGG,
    "sinc-vgg1d": SincNetVGG1D,
    "sinc-resnet": SincNetResNet,
    "sinc-tcn": SincNetTCN,
    "agafya": agafya.Agafya,
    "agape": agafya.Agape,  # Agafya without sinc layer
}

HYPER_PARAMS_SPACES = {
    "agafya": {
        "space": agafya.UtilsAgafya.hyper_param_space,
        "convert_func": agafya.UtilsAgafya.hyper_param_space_to_model_space,
        "max_evals": 64,
    },
    "agape": {
        "space": agafya.UtilsAgape.hyper_param_space,
        "convert_func": agafya.UtilsAgape.hyper_param_space_to_model_space,
        "max_evals": 64,
    },
}
