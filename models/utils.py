import pdb
import torch
import torch.nn as nn


class LayerNorm1D(nn.Module):
    def __init__(self, shape, eps=1e-6):
        super(LayerNorm1D, self).__init__()
        self.gamma = nn.Parameter(torch.ones(shape))
        self.beta = nn.Parameter(torch.zeros(shape))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.gamma * (x - mean) / (std + self.eps) + self.beta
