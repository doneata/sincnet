import torch.nn as nn

from torch.nn.modules.normalization import LayerNorm

from models.utils import LayerNorm1D


def conv1d(in_channels, out_channels, stride=1):
    return nn.Conv1d(
        in_channels, out_channels, kernel_size=3, stride=stride, padding=1, bias=False
    )


# Residual block
class ResidualBlock(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1, downsample=None):
        super(ResidualBlock, self).__init__()
        self.conv1 = conv1d(in_channels, out_channels, stride)
        self.bn1 = nn.BatchNorm1d(out_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv1d(out_channels, out_channels)
        self.bn2 = nn.BatchNorm1d(out_channels)
        self.downsample = downsample

    def forward(self, x):
        residual = x
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        if self.downsample:
            residual = self.downsample(x)
        out += residual
        out = self.relu(out)
        return out


def make_layers(layers_config, input_normalization=""):
    def make_layer(block, in_channels, nr_channels, nr_blocks, stride=1):
        downsample = None
        if stride != 1 or in_channels != nr_channels:
            downsample = nn.Sequential(
                conv1d(in_channels, nr_channels, stride=stride),
                nn.BatchNorm1d(nr_channels),
            )
        layers = []
        layers.append(block(in_channels, nr_channels, stride, downsample))
        for i in range(1, nr_blocks):
            layers.append(block(nr_channels, nr_channels))
        return nn.Sequential(*layers)

    layers = []

    input_size = (60, 118)
    in_channels = input_size[0]

    # Input normalization
    if input_normalization == "layer-norm":
        layers.append(LayerNorm(input_size))
    elif input_normalization == "layer-norm-1d":
        layers.append(LayerNorm1D(input_size))

    # Residual blocks
    for config in layers_config:
        layers.append(make_layer(ResidualBlock, in_channels, **config))
        in_channels = config["nr_channels"]

    return nn.Sequential(*layers)
