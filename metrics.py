from scipy.interpolate import interp1d

from scipy.optimize import brentq

from sklearn.metrics import roc_curve

from matplotlib import pyplot as plt

import numpy as np

import torch

from torch.nn import NLLLoss


DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def replicate_target(target, n):
    targets = [target] * n
    targets = torch.tensor(targets).long().to(DEVICE)
    return targets


class AverageMetric:
    def __init__(self, cost, percentage=True):
        self.cost = cost
        self.value = 0
        self.count = 0
        self.p = 100 if percentage else 1

    def update(self, log_probas, target):
        nr_elems, _ = log_probas.shape
        loss = self.cost(log_probas, replicate_target(target, nr_elems))
        loss = loss.detach().cpu().numpy()
        self.value += loss * nr_elems
        self.count += nr_elems

    def compute(self):
        return self.p * self.value / self.count


class SentenceErrorMetric:
    def __init__(self, percentage=True):
        self.value = 0
        self.count = 0
        self.p = 100 if percentage else 1

    def update(self, log_probas, target):
        pred = torch.mean(log_probas, dim=0).argmax()
        is_incorrect = target != pred
        self.value += int(is_incorrect)
        self.count += 1

    def compute(self):
        return 100 * self.value / self.count


class EERMetric:
    def __init__(self, percentage=True):
        self.labels = []
        self.scores = []
        self.p = 100 if percentage else 1

    def update(self, log_probas, target):
        labels = np.zeros(log_probas.shape[1])
        labels[target] = 1
        mean_log_probas = torch.mean(log_probas, dim=0)
        mean_log_probas = mean_log_probas.cpu().numpy().tolist()
        self.scores.extend(mean_log_probas)
        self.labels.extend(labels)

    def compute(self):
        fpr, tpr, _ = roc_curve(self.labels, self.scores, pos_label=1)
        return self.p * brentq(lambda x: 1 - x - interp1d(fpr, tpr)(x), 0.0, 1.0)

    def plot(self):
        fpr, tpr, _ = roc_curve(self.labels, self.scores, pos_label=1)
        plt.loglog(fpr, 1 - tpr)
        plt.savefig('/tmp/o.png')


neg_log_like = NLLLoss()


def error_cost(log_probas, targets):
    _, preds = torch.max(log_probas, dim=1)
    return torch.mean((preds != targets).float())


METRICS = {
    "loss": AverageMetric(neg_log_like, percentage=False),
    "error": AverageMetric(error_cost),
    "sent": SentenceErrorMetric(),
    "eer": EERMetric(),
}
