# Example usage:
# bash run_exp_stride_window.sh  31 
# bash run_exp_stride_window.sh  63
# bash run_exp_stride_window.sh 127
# bash run_exp_stride_window.sh 255
# bash run_exp_stride_window.sh 511

set -e
# set -x

if [[ $# -eq 0 ]] ; then
    echo 'please specify the window size'
    exit 0
fi

window=$(printf "%03d" $1)

for s in 2 4 8 16; do
    stride=$(printf "%02d" $s)
    key=hg-stride-${stride}-filter-size-${window} 
    python -u main.py -t train -m agafya -d timit-val --hyper-params-model ${key} --hyper-params-optim leo | tee logs/2019-03-28-agafya-${key}.log
    python -u main.py -t evaluate:test -m agafya -d timit-val --hyper-params-model ${key} --hyper-params-optim leo
done
