A refactored version of the speaker recognition code – SincNet.
The code facilitates the change of models, datasets, evaluation metrics.

# Examples

```bash
python main.py -t train -m sinc-mlp -d timit --hyper-params-model h
```

Running `hyperopt` to learn the hyper-parameters:

```bash
# Master
python main.py -t hyperopt -m agafya -d timit --hyper-params-optim cancer --hyper-params-model h
# Worker
PYTHONPATH=$PYTHONPATH:. hyperopt-mongo-worker --mongo=doneata@141.85.160.7:12321/sincnetdb --poll-interval=0.1 --workdir="."
```

See also the script `run_exp_stride_window.sh`.

# Set-up

Prepare the virtual environment:

```bash
virtualenv -p python3 venv
source venv/bin/activate
```

Install the requirements:

```bash
pip install -r requirements.txt
```

# Overview

The entry point is the `main.py` script.

Other important files and folders are:

```
├── config/
├── data.py
├── main.py
├── metrics.py
└── models/
```

# Models

The models are defined in the `models/` folder;
there are multiple models and they are listed in the `MODELS` dictionary from `__init__.py`.
The most relevant model is `agafya.Agafya` which corresponds to a sinc feature extractor followed by a one-dimensional residual network.

# Datasets

The datasets are specified in the `data.py` file.
Currently, there are implemented variants for the two datasets used in the SincNet paper, TIMIT and LibriSpeech.

1. `TimitDataset` being a split of TIMIT similar to what was used in the SincNet paper.
2. `Timit10Dataset` being a subset of TIMIT that uses only ten speakers; this variant is used mostly for testing and debugging purposes.
3. `TimitVal` being a split of TIMIT that uses a validation set – one of the five files for each speaker from training is used in the validation split.
4. `LibriSpeechVal` being a split of LibriSpeech that uses a validation set.

If you want to define a new dataset here is a basic template, with type annotations:

```python
class MyDataset:
    def __init__(self, split: str) -> None:
        pass

    def __str__(self) -> str:
        pass

    def get_file_names(self) -> Dict[str]:
        pass

    def get_paths_and_targets(self) -> List[Tuple[str, str]]:
        pass
```

Finally, add the newly created class to the dictionary of datasets:

```python
DATASETS = {
    "my-dataset": MyDataset,
}
```

# Metrics

The evaluation metrics are specified in the `metrics.py` file.
There are impemented four metrics:

- `loss` being the average negative log likelihood.
- `error` being the average frame error rate.
- `sent` being the average classification (per utterance) error rate.
- `eer` being the equal error rate.

# TODO

- [x] The number of clases parameter should be related to dataset and not to the model
