import pdb
import pprint

from torch.optim import Adam

import main


def f(hyper_choice, convert_hyper_params, hyper_params, dataset_name, model_name):
    def get(hyper_choice, type_):
        if type_ == "input":
            return {}
        elif type_ == "model":
            return convert_hyper_params(hyper_choice)
        elif type_ == "optim":
            return {"learning_rate": 10 ** hyper_choice["optim.learning_rate:log10"]}
        assert False

    print("Hyper-parameter choice")
    pprint.pprint(hyper_choice)

    # update state with hyper-parameter configuration
    for t in ("input", "model", "optim"):
        hyper_params[t].update(get(hyper_choice, t))

    print("Update hyper-parameters")
    pprint.pprint(hyper_params)

    model, optimizer, data = main.prepare_data_from_hyper_params(
        hyper_params, dataset_name, model_name
    )
    callbacks = (
        main.LoggerCallback(),
        main.FixedStopping(hyper_params["optim"]["nr-steps"]),
        main.LearningRateStopping(optimizer, min_lr=1e-6),
    )
    status = main.train(
        model, optimizer, data, hyper_params["optim"], callbacks=callbacks
    )

    print()
    print("Hyper-parameters :")
    pprint.pprint(hyper_choice)
    print("{:16s} : {}".format("loss", status["loss"]))

    return status
