import argparse
import dill
import itertools
import json
import os
import pdb
import random

from hyperopt import STATUS_OK, Trials, fmin, hp, tpe

from hyperopt.mongoexp import MongoTrials

from functools import partial

from math import ceil

from time import time

import numpy as np

import pprint

import soundfile

import torch

from torch.optim import Adam

from torch.optim.lr_scheduler import ReduceLROnPlateau

from torch.nn import NLLLoss

from torchsummary import summary

from tqdm import tqdm

from data import DATASETS

from metrics import METRICS

from models import HYPER_PARAMS_SPACES, MODELS, get_model_path

from utils import count_parameters, sliding_window

import objective


dill.settings["recurse"] = True

SEED = 1337
random.seed(SEED)
torch.manual_seed(SEED)

DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

COST = NLLLoss()


def pad_signal(signal, length):
    if len(signal) < length:
        return np.pad(signal, (0, length - len(signal)), "wrap")
    else:
        return signal


def get_batch_rand(paths_and_targets, batch_size, window_len, factor_amplitude):

    # Pre-allocate data
    data = np.zeros([batch_size, window_len])
    targets = np.zeros(batch_size)

    for i in range(batch_size):
        # Pick random file
        path, target = random.choice(paths_and_targets)
        signal, _ = soundfile.read(path)
        signal = pad_signal(signal, window_len)

        # Pick random window
        signal_len = signal.shape[0]
        s = random.randint(0, signal_len - window_len - 1)
        e = s + window_len

        # Pick random amplitude
        alpha = random.uniform(1 - factor_amplitude, 1 + factor_amplitude)

        data[i] = alpha * signal[s:e]
        targets[i] = target

    data = torch.tensor(data).float().to(DEVICE)
    targets = torch.tensor(targets).long().to(DEVICE)

    return data, targets


def evaluate_batch(model, cost, batch):
    data, targets = batch

    outputs = model(data)
    predictions = torch.argmax(outputs, dim=1)

    loss = cost(outputs, targets)
    error = torch.mean((predictions != targets).float())

    return loss, error


def save_predictions(filename, log_probas, paths_and_targets):
    nr_frames = [p.shape[0] for p in log_probas]
    nr_frames = np.array(nr_frames).astype("int")

    log_probas = np.vstack(log_probas)

    targets = [t for _, t in paths_and_targets]
    targets = np.array(targets).astype("int")

    with open(filename, "wb") as f:
        np.save(f, log_probas)
        np.save(f, nr_frames)
        np.save(f, targets)


class BestResult:
    def __init__(self):
        self.best_loss = np.inf

    def is_best(self, loss):
        return loss < self.best_loss

    def update(self, loss):
        self.best_loss = min(loss, self.best_loss)


class LoggerCallback:
    def __init__(self):
        self.HEADER = (
            "{:>5s} | {:>12s} | {:>12s} | {:>12s} | {:>12s} | {:>10s} | {:>10s} "
        )
        self.ROW = (
            "{:5d} | {:12.4f} | {:12.4f} | {:12.4f} | {:12.4f} | {:10s} | {:10.2f} "
        )

    def __call__(
        self,
        step,
        loss_train,
        error_train,
        loss_valid,
        error_valid,
        is_best,
        time,
        **kwargs
    ):
        if step == 1:
            print(
                self.HEADER.format(
                    "step",
                    "train loss",
                    "train error",
                    "valid loss",
                    "valid error",
                    "best",
                    "time (s)",
                )
            )
        best_str = "←" if is_best else "."
        print(
            self.ROW.format(
                step, loss_train, error_train, loss_valid, error_valid, best_str, time
            )
        )


class ModelCheckpoint:
    def __init__(self, key):
        self.key = key

    def __call__(self, model, is_best, **kwargs):
        if is_best:
            torch.save(model.state_dict(), get_model_path(self.key))


class FixedStopping:
    def __init__(self, nr_iterations):
        self.nr_iterations = nr_iterations

    def __call__(self, step, **kwargs):
        if step >= self.nr_iterations:
            raise StopIteration


class LearningRateScheduler:
    def __init__(self, optimizer, patience):
        self.scheduler = ReduceLROnPlateau(
            optimizer, patience=patience, verbose=True, factor=0.25
        )

    def __call__(self, error_valid, **kwargs):
        self.scheduler.step(error_valid)


class LearningRateStopping:
    def __init__(self, optimizer, min_lr):
        self.optimizer = optimizer
        self.min_lr = min_lr

    def _get_lr(self):
        params, = self.optimizer.param_groups
        return params["lr"]

    def __call__(self, **kwargs):
        if self._get_lr() < self.min_lr:
            raise StopIteration


def train(model, optimizer, data, hyper_params_opt, callbacks=tuple()):

    # Unpack data
    get_batch_train, batches_valid = data
    nr_steps = hyper_params_opt["checkpoint-freq"]

    time_prev = time()
    best_result = BestResult()

    is_checkpoint = lambda step: step % nr_steps == 0
    results_to_numpy = lambda res: [r.data.cpu().numpy() for r in res]

    # Initialize
    model.train()
    loss_sum = 0
    error_sum = 0

    states = []
    progress_bar = tqdm(total=nr_steps, leave=False)

    for step in itertools.count(1):

        batch_train = get_batch_train()
        loss, error = evaluate_batch(model, COST, batch_train)

        model.zero_grad()
        loss.backward()
        optimizer.step()

        loss_sum = loss_sum + loss.detach()
        error_sum = error_sum + error.detach()

        progress_bar.update()

        if is_checkpoint(step):

            model.eval()

            losses_and_errors = [
                results_to_numpy(evaluate_batch(model, COST, batch_valid))
                for batch_valid in batches_valid
            ]
            losses, errors = zip(*losses_and_errors)

            loss_train = loss_sum / nr_steps
            error_train = error_sum / nr_steps

            loss_train = loss_train.cpu().tolist()
            error_train = error_train.cpu().tolist()

            loss_valid = np.mean(losses)
            error_valid = np.mean(errors)

            time_curr = time()
            time_delta = time_curr - time_prev
            time_prev = time_curr

            # Reset
            model.train()
            loss_sum = 0
            error_sum = 0

            state = {
                "step": step,
                "error_train": error_train,
                "error_valid": error_valid,
                "loss_train": loss_train,
                "loss_valid": loss_valid,
                "is_best": best_result.is_best(error_valid),
                "time": time_delta,
            }

            best_result.update(error_valid)
            states.append(state)

            try:
                for callback in callbacks:
                    callback(model=model, **state)
            except StopIteration:
                break

            progress_bar.close()
            progress_bar = tqdm(total=nr_steps, leave=False)

    return {
        "loss": best_result.best_loss,
        "status": STATUS_OK,
        "states": states,
        "nr-parameters": count_parameters(model),
    }


def prepare_data_from_hyper_params(
    hyper_params, dataset_name: str, model_name: str, return_only_model=False
):
    """Returns data needed for training based on hyper-parameters. This
    function is reused in "objective.py", for hyper-parameter optimization.

    """

    hyper_params_input = hyper_params["input"]
    hyper_params_model = hyper_params["model"]
    hyper_params_optim = hyper_params["optim"]

    Dataset = DATASETS[dataset_name]

    window_len = int(
        hyper_params_input["freq"] * hyper_params_input["duration-time"] / 1000
    )

    hyper_params_input["duration-frames"] = window_len
    hyper_params_input["shift-frames"] = int(
        hyper_params_input["freq"] * hyper_params_input["shift-time"] / 1000
    )

    input_params = {
        k: v for k, v in hyper_params_input.items() if k in ("freq", "duration-frames")
    }
    output_params = {"nr-classes": Dataset.NR_CLASSES}

    model = MODELS[model_name](input_params, output_params, hyper_params_model)
    model.to(DEVICE)

    summary(model, (window_len,))

    if return_only_model:
        return model

    optimizer = Adam(
        model.parameters(),
        lr=hyper_params_optim["learning_rate"],
        # eps=hyper_params_optim["eps"],
        # alpha=hyper_params_optim["alpha"],
    )

    # Fix hyper-parameters
    get_batch_rand1 = lambda split: get_batch_rand(
        Dataset(split).get_paths_and_targets(),
        hyper_params_optim["batch-size"],
        hyper_params_input["duration-frames"],
        hyper_params_input["factor-amplitude"],
    )

    # Prepare data for training and validation.
    # The asymmetry between training and validation appears because for
    # validation we want to keep the same windows for all checkpoints.
    get_batch_train = lambda: get_batch_rand1("train")
    batches_valid = [
        get_batch_rand1("valid") for _ in range(hyper_params_optim["nr-batches-valid"])
    ]
    data = (get_batch_train, batches_valid)

    return model, optimizer, data


def evaluate(model, model_key, paths_and_targets, metrics, hyper_params_window):

    BATCH_SIZE = 128

    model.eval()

    get_windows = partial(
        sliding_window,
        size=hyper_params_window["duration-frames"],
        stepsize=hyper_params_window["shift-frames"],
    )

    def pad(array):
        nr_elems = len(array)
        pad_size = ceil(nr_elems / BATCH_SIZE) * BATCH_SIZE - nr_elems
        return np.pad(
            array, ((0, pad_size), (0, 0)), mode="constant", constant_values=0
        )

    def split(array):
        return np.split(array, len(array) // BATCH_SIZE)

    def to_cuda(data):
        return torch.tensor(data).float().to(DEVICE)

    def predict(signal):
        windows = get_windows(signal)
        nr_windows = len(windows)
        predictions = [model.forward(to_cuda(b)) for b in split(pad(windows))]
        predictions = torch.cat(predictions)[:nr_windows]
        return predictions

    def read_wrd(path):
        def p(row):
            t0, t1, word = row.split()
            return int(t0), int(t1), word
        with open(path, 'r') as f:
            return [p(row) for row in f.readlines()]

    def trim_signal(signal, path):
        path_wrd = path.replace('wav', 'wrd')
        if os.path.exists(path_wrd):
            times_words = read_wrd(path_wrd)
            a, *_, z = times_words
            beg = a[0]
            end = z[1]
            return signal[beg: end]
        else:
            return signal

    def get_result(path_and_target):
        path, target = path_and_target

        signal, _ = soundfile.read(path)
        signal = trim_signal(signal, path)
        signal = pad_signal(signal, hyper_params_window["duration-frames"])

        log_probas = predict(signal)

        for metric in metrics.values():
            metric.update(log_probas, target)

        log_probas = log_probas.cpu().numpy()
        return log_probas

    with torch.no_grad():
        time_start = time()
        log_probas = list(map(get_result, tqdm(paths_and_targets)))
        time_delta = time() - time_start

    print("Elapsed time:", time_delta)

    for k, metric in metrics.items():
        print(k, metric.compute())

    return log_probas


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t",
        "--todo",
        type=str,
        choices=["train", "hyperopt", "evaluate:valid", "evaluate:test"],
        required=True,
        help="choose the task to do",
    )
    parser.add_argument(
        "-m",
        "--model",
        type=str,
        choices=MODELS,
        required=True,
        help="which model to use",
    )
    parser.add_argument(
        "-d",
        "--dataset",
        type=str,
        choices=DATASETS,
        required=True,
        help="choose a dataset",
    )
    parser.add_argument(
        "--hyper-params-model",
        default="h",
        help="choice of model hyper-parameters (named after chemical elements)",
    )
    parser.add_argument(
        "--hyper-params-optim",
        default=None,
        help="choice of optimization hyper-parameters (named after stars)",
    )
    parser.add_argument(
        "--hyper-params-input",
        default=None,
        help="choice of input hyper-parameters (named after colors)",
    )
    args = parser.parse_args()

    print(args)

    def get_model_key():
        model_key = [args.model, args.dataset, args.hyper_params_model]
        if args.hyper_params_optim:
            model_key.append(args.hyper_params_optim)
        if args.hyper_params_input:
            model_key.append(args.hyper_params_input)
        return "-".join(model_key)

    GET_KEY = {
        "model": args.model + "-" + args.hyper_params_model,
        "optim": args.hyper_params_optim,
        "input": args.hyper_params_input,
    }

    def get_hyper_params(type_):
        path = os.path.join("config", type_, GET_KEY[type_] + ".json")
        with open(path, "r") as f:
            return json.load(f)

    if args.dataset.startswith("timit"):
        nr_batches_valid = 64
    elif args.dataset.startswith("libri"):
        nr_batches_valid = 256
    else:
        assert False, "Unknown dataset"

    # Default hyper-parameters for the input window
    HYPER_PARAMS_INPUT = {
        "freq": 16000,
        "duration-time": 200,  # ms
        "shift-time": 10,  # ms
        "factor": 1,
        "factor-amplitude": 0.2,
    }

    # Default hyper-parameters for optimization
    HYPER_PARAMS_OPTIM = {
        "learning_rate": 0.001,
        "alpha": 0.95,
        "eps": 1e-8,
        "batch-size": 128,
        "nr-steps": 65536,
        "nr-batches-valid": nr_batches_valid,
        "checkpoint-freq": 512,
    }

    if args.hyper_params_optim:
        d = get_hyper_params("optim")
        HYPER_PARAMS_OPTIM.update(d)

    if args.hyper_params_input:
        d = get_hyper_params("input")
        HYPER_PARAMS_INPUT.update(d)

    hyper_params_model = get_hyper_params("model")

    hyper_params = {
        "input": HYPER_PARAMS_INPUT,
        "model": hyper_params_model,
        "optim": HYPER_PARAMS_OPTIM,
    }

    model_key = get_model_key()

    if args.todo == "train":
        pprint.pprint(hyper_params)
        model, optimizer, data = prepare_data_from_hyper_params(
            hyper_params, args.dataset, args.model
        )
        callbacks = (
            LoggerCallback(),
            ModelCheckpoint(model_key),
            LearningRateScheduler(optimizer, patience=5),
            FixedStopping(HYPER_PARAMS_OPTIM["nr-steps"]),
            LearningRateStopping(optimizer, min_lr=1e-6),
        )
        result = train(
            model, optimizer, data, hyper_params["optim"], callbacks=callbacks
        )
        print(result["loss"])

    if args.todo == "hyperopt":
        trials = MongoTrials(
            "mongo://doneata@141.85.160.7:12321/sincnetdb/jobs", exp_key=model_key
        )
        # trials = Trials()
        best = fmin(
            partial(
                objective.f,
                convert_hyper_params=HYPER_PARAMS_SPACES[args.model]["convert_func"],
                hyper_params=hyper_params,
                dataset_name=args.dataset,
                model_name=args.model,
            ),
            space=HYPER_PARAMS_SPACES[args.model]["space"],
            max_evals=HYPER_PARAMS_SPACES[args.model]["max_evals"],
            algo=tpe.suggest,
            trials=trials,
            show_progressbar=False,
        )
        print(best)

    if args.todo in ("evaluate:valid", "evaluate:test"):
        _, split = args.todo.split(":")

        # Prepare dataset
        Dataset = DATASETS[args.dataset]
        dataset = Dataset(split)
        paths_and_targets = dataset.get_paths_and_targets()

        # Prepare model
        model = prepare_data_from_hyper_params(
            hyper_params, args.dataset, args.model, return_only_model=True
        )
        model.load_state_dict(torch.load(get_model_path(model_key)))

        predictions = evaluate(
            model, model_key, paths_and_targets, METRICS, HYPER_PARAMS_INPUT
        )
        filename = os.path.join("predictions", str(dataset), model_key + ".npy")
        save_predictions(filename, predictions, paths_and_targets)


if __name__ == "__main__":
    main()
